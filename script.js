function enviarMsg() {
  let Fim = document.getElementById("#inputMensagem");
  let msg = Fim.value;
  console.log(msg);

  let Msg = document.getElementById("#Msg");

  let Add  = document.createElement("div");
  Add.className = "mensagens";

  Add.innerHTML = `
    <div class="mensagem">
      <div class="texto">
        ${msg}
      </div>
      <div class="btnMensagem">
        <button class="Editar" onclick="editarMensagem(this)">Editar</button>
        <button class="Excluir" onclick="excluirMensagem(this)">Excluir</button>
      </div>
    </div>
  `;

Msg.appendChild(Add);

  Fim.value = "";
}
function excluirMensagem( excluir ) {
  let mensagemParaExcluir = excluir.closest(".mensagens");
  mensagemParaExcluir.remove();
}

document.addEventListener("DOMContentLoaded", function() {
  let envio = document.getElementById("#Enviar");
  envio.addEventListener("click", enviarMsg);
});

